# Exécuter ou tester des processus graphiques depuis son shell.

Dans la documentation précédente nous avons configuré wsl. Or, il se peut qu'il soit nécessaire d'éxecuter des programmes graphiques depuis son shell surtout s'il faut développer une interface graphique.
Si on a de la chance c'est un processus web et il suffit de l'exécuter et se connecter au port sur lequel il est affiché. `localhost:port` sur wsl normal ou `ifconfig` pour trouver l'addresse ip et `ADDRESSE_IP:port` sur wsl2.

Une autre possibilité s'offre à ceux qui veuelent ouvrir un programme directement comme l'explorateur de fichier _nautilus_ ou le lecteur de pdf _evince_ou même un programme écrit par soi-même. Dans ce cas, il faut que la sortie graphique soit configurée pour "ouvrir" des fenêtres pas dans le sous système mais sous windows direcement. Pour se faire quelques étapes sont nécessaires:

1. Installer un serveur _xserver_ pour windows [vcxserv](https://sourceforge.net/projects/vcxsrv/) open source fait parfaitement l'affaire et est encore relativement léger. 
2. Installer ssh server dans wsl:
```shell
sudo apt-get remove  openssh-server

sudo apt-get install  openssh-server
```
3. Editer le fichier `/etc/ssh/sshd_config` : 
```
Changer - PermitRootLogin no
Ajouter - AllowUsers yourusername
Changer - PasswordAuthentication yes
Ajouter - UsePrivilegeSeparation no
Changer - ListenAddress 0.0.0.0
```

4. Executer la commande *(Important ce service doit être démarré dans n'importe quel shell auquel vous voulez vous connecter pour que ce soit automatique vous pouvez ajouter cette commande ou `--start`au fichier .bashrc comme dans le tuto précédent.)* :
```sudo service ssh --full-restart```
5. Assurez vous d'avoir exporté le display de destination `export DISPLAY=:0.0` sur wsl classique se connecte au localhost. Sur wsl2 s'est un peu plus tricky il faut créer une session ssh avec le forwarding x11 activé un bon client pour se faire est putty. . C'est aussi possiible de récupérer l'addresse IP de l'hôte à la place de localhost et de s'y connecter à la place de localhost si les settings réseaux le permettent. Je ne décrirais pas plus ici car considérant que si vous utilisez wsl2 qui est encore expérimental vous serez capables de configurer tout ça (Important désactiver l'opengl natif sur vcxserv au lancement car il peut poser problème.)
6. Lancer vcxserv
7. Lancer l'outil graphique depuis la ligne de commande et il se connectera à votre X serveur.
8. Bienvenue dans un nouveau monde 