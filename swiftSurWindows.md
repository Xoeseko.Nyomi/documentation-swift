# Environnement de développement swift sous windows
***Attention: Cet environnement de développement est très expérimental. En l'utilisant je suis conscient de ce fait et en assume les risques. Si ça impact en quoi que ce soit ma productivité, je sais que je suis capable de débugger moi même. Je n'offre aucune garantie quelle qu'elles soient. C'est donc plus sûr d'utiliser un environnement supporté comme Ubuntu ou MacOSX***

Cela dit, pour les quelques fous qui veulent encore perséverer avec plusieurs solutions expérimentales (WSL est en Beta, la version 5.1 de Swift est "nécessaire", le plugin pour VS code est inofficiel a zéro étoiles et seulement 800 téléchargements à ce jour).

## Introduction
L'environnement de développement présenté repose sur une fonctionnalité du nom de LSP pour language server protocol. Ce protocol permet de récuperer depuis un serveur les spécificités d'un langage, voir, d'interpréter un programme directement, sans devoir installer des tonnes de baggages localement sur son ordinateur. Cette manière de fonctionner est courante typiquement dans la communauté scala. Un avantage de cette méthode, en plus de la légèreté est que on a toujours la dernière version du langage puisque le serveur est tenu à jour.

## Installation
0. Mettre à jour les repos d'installation et les dépendences avec `sudo apt-get update && sudo apt-get upgrade`
1. Installer le sous-système windows pour linux (https://docs.microsoft.com/fr-fr/windows/wsl/install-win10)
2. Installer VS Code ainsi que l'extension "Remote - WSL" et "Swift Language".
3. Dans le sous-système pour linux installer la version 5.1 de swifT
    ```bash
    # 3.1 Installer les dépendences
    sudo apt install libicu-dev clang

    # 3.2 Télécharger l'archive swift depuis le serveur officiel puis l'extraire et déplacer le dossier contenu dans un endroit plus convenable
    wget https://swift.org/builds/swift-5.1-release/ubuntu1804/swift-5.1-RELEASE/swift-5.1-RELEASE-ubuntu18.04.tar.gz
    tar xzf swift-5.1-RELEASE-ubuntu18.04.tar.gz
    sudo mv swift-5.1-RELEASE-ubuntu18.04 /usr/local/

    # 3.3 Il faut ajouter les programmes d'interprétation, de compilation, de déboguage et autres au chemin d'exécution. En ajoutant cette ligne au fichier .bashrc ils seront remis à chaque fois que l'on lance le terminal linux
    echo 'export PATH=/usr/local/swift-5.1-RELEASE-ubuntu18.04/usr/bin:$PATH' >> .bashrc

    # Recharger le shell avec le nouvel environnement
    source .bashrc

    # Tester son installe de swift les deux premières devraient rendre la version installée dans mon cas :
    # Swift version 5.1-RELEASE
    # Target: x86_64-unknown-linux-gnu
    swift --version
    # ou
    swiftc --version
    
    # ou bien testez en exécutant un fichier swift que vous auriez écrit
    swift helloWord.swift
    ```

4. La dernière étape est de lier VS Code à l'environnement que l'on a mis en place. 

    4.1 Dans VS Code, utiliser l'extension "Remote - WSL" pour se connecter au sous système tout en bas à gauche ou en appuyant sur le touche `f1` et chercher WSL.

    4.2 Une fois l'éditeur ouvert dans le sous-système, il faut encore installer une dernière extension "SourceKit-LSP - Unofficial CI build" de Pavel Vasek.

    4.3 Finalement, il faut configurer le serveur qui sera simplement `sourcekit-lsp` si vous l'avez ajouté au chemin d'exécution. Et le chemin d'accès à swift devrait être `/usr/local/swift-5.1-RELEASE-ubuntu18.04/`. Ces 2 réglages se trouvent dans `File > Preferences > Settings` puis sous `Extensions > SourceKit-LSP`
    
    4.4 `ctrl+shift+p` puis `reload window` pour appliquer les changements

## Fonctionalités

### Build & Run
À compléter

### Autocompletion
L'autocompletion se fait de manière naturelle avec `ctrl+espace`. C'est un bon moyen de tester si tout est bien installé.

### Dépendences locales
Pour que les dépendences locales soient indexées, il est nécessaire de build le projet duquel elles font parties. C'est le cas par exemple lorsque nous recevons des librairies pour un TP. On peut facilement lire les fichiers de la libraire pour savoir ce qui est à disposition. Cependant, pour que le serveur reconnaisse les librairies automatiquement, il va être nécessaire de build le projet à chaque fois que les dépendences devraient changer. La commande pour cela est : `swift build`

## Références
https://microsoft.github.io/language-server-protocol/

https://nshipster.com/vscode/

https://solarianprogrammer.com/2017/04/19/getting-started-swift-windows-subsystem-linux/
